import requests


class MyToken:
    cookies = {
        '_by_l_g_d': '6f0a8846-a583-c05d-ce86-1ca81e808fef',
        '_ga': 'GA1.1.1287956896.1672390602',
        '_tt_enable_cookie': '1',
        '_ttp': '8_aRh9x5SFjM92tEUKvtR7_uyMO',
        '_ga_LEBL6PF94W': 'GS1.1.1672847849.3.0.1672847849.0.0.0',
        'deviceId': '68889353-3359-6bb5-1e14-98fea5d103b0',
        '_ym_uid': '1673506064103162146',
        '_abck': 'EA5A1B02BFB42DF82A0542F4F23EBCC8~0~YAAQvd5FaMQUZf+JAQAAquiOKAqlOtUPFPz1f19DCq2FOpowENFMU2lcVwSXEcABV+TPHdJwZ+lmwuYHhauriCGubl51t0qWGkAS7F4CbfGBy8LuxtAaVobbAJnwAiKlHdkwC4U0j5VQ3CPPa8Tx8b+PcJQzArkXUI1MBoMTl863uHlbATXTb/pf6GT5VMiPVsj0+byD2pxtsAQyHLIG682bS62ORz0HVEiiqetDzY6m7vzCLvVp0ir/d0V0xuRRnnuRJdxNmuFHoNDBKAwoCuq/tOudSpnIN23W36Om7Taxlo/vRs89ujuYCoAocAgq2TVtYqJuNwoI2tPA/qQBlSGsVhU6xan6AkFU1nM9naJgZ+R+YXnVk4dSEhSmB14d35I2SD7lfV9dCUpqBzZ5EFcOo3vn6N8=~-1~-1~-1',
        'bm_sz': '76D68774D72839933801C06B09993CBD~YAAQvd5FaMcUZf+JAQAAq+iOKBS6haFhB14gHkmtPWNKI/EOKsbM2qACVgaL138YmN2gfb2k8JkDy0kNsvtwOtntGp1fWRNmyTco2z493gyCyhsuPsr5MUc37g2oIrqxzSUa5ju5gh8sznVwChy9fPbN+nt7kMYoTQHDNmcPN/eD/tMcdyYXJ9vyhQEFsNNG/mG3Znpm2ovF06U2/GKcTRMFjE/12P0zCLPELK4AAQrXOVqwvfPoorpwzGdzKT6hLBTK4PusMKwjHIsgyhaAT91sCks0eiuGY/D1dtlLQFF8QQ==~3356230~3487558',
        'bm_mi': '2F8D7F5833459ADBABA737D0C9F60211~YAAQvd5FaPsUZf+JAQAAnfCOKBSFuGx9PLZJFgz7e8l5oxLDmMojpkt/V+bdd8eokK09ZxB5VEmr5+J22nBZGAE9n3nmdOnJ31fBA7ifVXAc2xGai2DvC1QIfB5IrOyy9mgp/vruywkIZmPK1J/EVL2Kgmb8X6FIFnb7rc+6XdVQIu9nwd8eGRjl5q5i4gZolhwkKSZC1XXkvp4eAs91fqShFZrQ1bMfTGvGYu4zFm1tll5SFI77wFmsKROaWV3iUTWmcpJMpiH95o3cOK5BdKXmnMI+6fwoFq57hUyc30gxfBaK9YqBNr1vbT67hWh+ZcsyCEntNblo7g==~1',
        'BYBIT_REG_REF_prod': '{"lang":"ru-RU","g":"6f0a8846-a583-c05d-ce86-1ca81e808fef","referrer":"www.bybit.com/register?affiliate_id=59904&group_id=0&group_type=1&gclid=Cj0KCQjw_5unBhCMARIsACZyzS32w95rwk_xBb12x7g1KRr2jx5ekZmtyWx-uES79QgK6aXi-3DrfjIaAueGEALw_wcB","source":"bybit.com","medium":"affiliate","affiliate_id":"59904","group_id":"0","group_type":"1","url":"https://www.bybit.com/en-US/sign-up?affiliate_id=59904&group_id=0&group_type=1&gclid=Cj0KCQjw_5unBhCMARIsACZyzS32w95rwk_xBb12x7g1KRr2jx5ekZmtyWx-uES79QgK6aXi-3DrfjIaAueGEALw_wcB"}',
        'ak_bmsc': '782470D905078692CBD2DAFB2F357706~000000000000000000000000000000~YAAQvd5FaFQVZf+JAQAAc/eOKBRxzCp/eicpq9QoJT9KXE9rFEDzdtgZVH8SiR9v7yh+2qXfCGMEAesO1S4+W9HgAadCpeLYxqC5QkQv2mNVjPALvgMclV7KjbhA1+q/FfUphmRSwElgADH72OjH3yjOm0VrB2E80Bg6zIV407RyeielHPYFag7N/DktZQ7O5Iq6GCud1LDj/AyGajBKK6JGJ6AxUEm87YUoDmrNVH2+jF89GMdO3nUUgLAZOTmgyPjBkeU3neaHQ6Y8vNk6i5l39EZQer6isLf6V9FDrNvz2EFv5GwnA0kJflwHV+ZBU7w0QFeso2/Wfc7wOepINQWiW7cvsFu8I8HJR8ljtKTi2X3JtnMqnpKsvE57VYVFMAi7BiNqUMCL/gcdjF9YXGBKRkP0Y727Q8/Z0dPLAluGfZnHazhX379paQiE8vLZTNVvXlM+/lWNezQ8Q+vqu/W7y6QAIY8SSK4WuYWNwKB0AlsUGCO7VAOYH60iWPnqmLtXgKN4wWrkuA9h6u7IPtF20bk6eMwImYk=',
        '_gcl_aw': 'GCL.1692897584.Cj0KCQjw_5unBhCMARIsACZyzS32w95rwk_xBb12x7g1KRr2jx5ekZmtyWx-uES79QgK6aXi-3DrfjIaAueGEALw_wcB',
        '_gcl_au': '1.1.505934195.1692897584',
        '_ym_d': '1692897585',
        '_ym_isad': '1',
        'b_t_c_k': '',
        'sensorsdata2015jssdkcross': '%7B%22distinct_id%22%3A%2250378096%22%2C%22first_id%22%3A%22185623f5a3bba2-08a380bcc96dc68-26021151-1327104-185623f5a3cf76%22%2C%22props%22%3A%7B%22%24latest_traffic_source_type%22%3A%22%E7%9B%B4%E6%8E%A5%E6%B5%81%E9%87%8F%22%2C%22%24latest_search_keyword%22%3A%22%E6%9C%AA%E5%8F%96%E5%88%B0%E5%80%BC_%E7%9B%B4%E6%8E%A5%E6%89%93%E5%BC%80%22%2C%22%24latest_referrer%22%3A%22%22%2C%22_a_u_v%22%3A%220.0.5%22%7D%2C%22identities%22%3A%22eyIkaWRlbnRpdHlfY29va2llX2lkIjoiMTg1NjIzZjVhM2JiYTItMDhhMzgwYmNjOTZkYzY4LTI2MDIxMTUxLTEzMjcxMDQtMTg1NjIzZjVhM2NmNzYiLCIkaWRlbnRpdHlfbG9naW5faWQiOiI1MDM3ODA5NiJ9%22%2C%22history_login_id%22%3A%7B%22name%22%3A%22%24identity_login_id%22%2C%22value%22%3A%2250378096%22%7D%2C%22%24device_id%22%3A%22185623f5a3bba2-08a380bcc96dc68-26021151-1327104-185623f5a3cf76%22%7D',
        '_ga_SPS4ND2MGC': 'GS1.1.1692897584.18.1.1692898256.0.0.0',
        'bm_sv': '6BAF31C9E7CB10E0DD60928684A602CC~YAAQvd5FaGVqZf+JAQAAwWOZKBQjrmFJMXnJ9EJRcQvv20wv8QaLMuTvWg+4bdTRyfmHt8rSoOJk3+c9HbAmmwlbmShibr73fm1pPg9p36iiSUvUKNJaAhMEhJ6xDuZgAShP91Gdz59Q1cymhHzSFnbaH2NTmWilg3l0lCVvBEuXXUGgjNbhLsxHXeSeylm9vuU7Thy5+R126RXiOWpJBNNvC9SBsycfdSa1siu8tKuTF/oPH/wQKa80d94PD9z7vw==~1',
    }

    headers = {
        'authority': 'api2.bybit.com',
        'accept': 'application/json',
        'accept-language': 'ru-RU',
        'content-type': 'application/json;charset=UTF-8',
        'guid': '6f0a8846-a583-c05d-ce86-1ca81e808fef',
        'lang': 'ru-RU',
        'origin': 'https://www.bybit.com',
        'platform': 'PC',
        'referer': 'https://www.bybit.com/',
        'risktoken': 'v2:gRoGleILbXZhkH5kO+txVr5CADtoRsAgt/XOWdylPZEEiqssNAeNQ1sZTD7FFut4EfnKn7XCkoAeMyfo8sMrsqWyF5ZWDpdfx+RcnGOxB+5V24uaoApdIfeArEfBqoiOwdygVPFUrjnDwcvv95aJ7BwHIBNAumuR53H7gezPyYZvN/mvt6dtwePPQSWWXdFz/xnAlq30ptxoKac=',
        'sec-ch-ua': '"Chromium";v="116", "Not)A;Brand";v="24", "Google Chrome";v="116"',
        'sec-ch-ua-mobile': '?0',
        'sec-ch-ua-platform': '"Windows"',
        'sec-fetch-dest': 'empty',
        'sec-fetch-mode': 'cors',
        'sec-fetch-site': 'same-site',
        'traceparent': '00-2e4c62a1866464dc6e8a408942899b84-835d80f578f263a8-00',
        'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/116.0.0.0 Safari/537.36',
    }

    json_data = {
        'userId': '',
        'tokenId': 'USDT',
        'currencyId': 'KGS',
        'payment': [],
        'side': '1',
        'size': '10',
        'page': '1',
        'amount': '',
        'authMaker': False,
        'canTrade': False,
    }

    payments = {
        '291': 'Optima Bank',
        '14': 'Bank Transfer',

    }

    def get_USDT_RUB(self) -> list:
        self.json_data['currencyId'] = 'RUB'
        p2p = []
        try:
            response = requests.post('https://api2.bybit.com/fiat/otc/item/online',
                                     cookies=self.cookies,
                                     headers=self.headers,
                                     json=self.json_data).json()

            for i in response['result']['items']:
                a = {}
                a['nickName'] = i['nickName']
                a['tokenId'] = i['tokenId']
                a['currencyId'] = i['currencyId']
                a['price'] = i['price']
                a['lastQuantity'] = i['lastQuantity']
                p2p.append(a)

            return p2p

        except:
            return p2p

    def get_USDT_KSG(self):
        self.json_data['currencyId'] = 'KGS'
        p2p = []
        try:
            response = requests.post('https://api2.bybit.com/fiat/otc/item/online',
                                     cookies=self.cookies,
                                     headers=self.headers,
                                     json=self.json_data).json()

            for i in response['result']['items']:
                a = {}
                a['nickName'] = i['nickName']
                a['tokenId'] = i['tokenId']
                a['currencyId'] = i['currencyId']
                a['price'] = i['price']
                a['lastQuantity'] = i['lastQuantity']
                p2p.append(a)

            return p2p

        except:
            return p2p



