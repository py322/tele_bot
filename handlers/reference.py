from aiogram import types, Dispatcher
from aiogram.utils.deep_linking import _create_link

from config import bot
from database.SQLOperator import SQLOperator
import keybords.kb as kb

import binascii
import os


async def reference_call_reference(call: types.CallbackQuery):
    await bot.send_message(
        chat_id=call.message.chat.id,
        text='Our referral program implies the accrual of 🎁100 points for each invited user!',
        reply_markup=await kb.three_button_inline_markup(
            text=['🔗 reference link', '📃 reference users', '💲 referral balance'],
            callback=['refer_btn_link', 'refer_btn_refer_list', 'refer_btn_balance']))


async def reference_call_link(call: types.CallbackQuery):
    referral = await SQLOperator().async_get_referral(by_user_id=call.from_user.id)
    if referral:
        text = f'reference link: `{referral["reference_link"]}`'
    elif not referral:
        token = binascii.hexlify(os.urandom(4)).decode()
        link = await _create_link(link_type='start', payload=token)
        text = f'reference link: `{link}`'

        await SQLOperator().async_insert_into('reference_balance', (
            call.from_user.id,
            link,
            0))
    else:
        text = 'Something is wrong, try again later...'

    await bot.send_message(
        chat_id=call.message.chat.id,
        text=text,
        parse_mode=types.ParseMode.MARKDOWN_V2,
        reply_markup=await kb.one_button_inline_markup(
            text='🔙 Back', callback='start_btn_refer'))


async def reference_call_referable_list(call: types.CallbackQuery):
    reference_list = await SQLOperator().async_get_reference_list(user_id=call.from_user.id)
    if reference_list:
        text = '\n'.join([i['username'] for i in reference_list])
    else:
        text = 'You have no any referable users!'
    await bot.send_message(
        text=text,
        chat_id=call.message.chat.id,
        reply_markup=await kb.one_button_inline_markup(
            text='🔙 Back', callback='start_btn_refer'))


async def reference_call_referral_balance(call: types.CallbackQuery):
    referral = await SQLOperator().async_get_referral(by_user_id=call.from_user.id)
    if referral:
        balance = referral['referral_balance']
    else:
        balance = 0
    await bot.send_message(
        text=f'Your balance: {balance} points',
        chat_id=call.message.chat.id,
        reply_markup=await kb.two_button_inline_markup(
            text=['🔙 Back', 'Transactions'],
            callback=['start_btn_refer', 'transactions_btn_transactions']))


def register_reference_handlers(dp: Dispatcher):
    dp.register_callback_query_handler(reference_call_reference, lambda call: call.data == 'start_btn_refer')
    dp.register_callback_query_handler(reference_call_link, lambda call: call.data == 'refer_btn_link')
    dp.register_callback_query_handler(reference_call_referable_list, lambda call: call.data == 'refer_btn_refer_list')
    dp.register_callback_query_handler(reference_call_referral_balance, lambda call: call.data == 'refer_btn_balance')
