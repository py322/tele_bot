from aiogram import types, Dispatcher
from aiogram.dispatcher import FSMContext
from config import bot
from database.SQLOperator import SQLOperator
from aiogram.dispatcher.filters.state import State, StatesGroup


class MyFormStates(StatesGroup):
    name = State()
    bd = State()
    bio = State() # Это фазы для хранения промежуточных ответов
    photo = State()
