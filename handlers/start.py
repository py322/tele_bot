import asyncio
from aiogram import types, Dispatcher
from aiogram.utils.deep_linking import _create_link

from config import bot
from database.SQLOperator import SQLOperator
import keybords.kb as kb


async def start_command_start(message: types.Message):
    await SQLOperator().async_insert_into('users', (
        message.from_user.id,
        message.from_user.first_name,
        message.from_user.last_name,
        message.from_user.username))

    _, token = message.get_full_command()
    if token:
        link = await _create_link(link_type='start', payload=token)
        referral = await SQLOperator().async_get_referral(by_link=link)
        if referral:
            in_base = await SQLOperator().async_references_in_pair(_from=referral['referral_telegram_id'], to=message.from_user.id)
            if not in_base:
                await SQLOperator().async_set_reference_pair(_from=referral['referral_telegram_id'], to=message.from_user.id)
                await SQLOperator().async_set_referral_balance(user_id=referral['referral_telegram_id'])

    await message.reply('Hello')
    await asyncio.sleep(2)
    await bot.send_message(
        chat_id=message.chat.id,
        text='Look at my possibilities:',
        reply_markup=await kb.four_button_inline_markup(
            text=['Setlist', 'Reference', 'Articles', 'Later'],
            callback=['start_btn_yes', 'start_btn_refer', 'start_btn_articles', 'start_btn_later']))


async def start_command_name(messages: types.Message):
    await messages.answer(f'Yor name: {messages.from_user.username}')


def register_start_handlers(dp: Dispatcher):
    dp.register_message_handler(start_command_start, commands=['start', 'hello', 'hi'])
    dp.register_message_handler(start_command_name, commands=['name'])
