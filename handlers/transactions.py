import binascii
import os

from aiogram.utils.deep_linking import _create_link
from aiogram import types, Dispatcher
from aiogram.dispatcher.filters.state import State, StatesGroup
from aiogram.dispatcher import FSMContext

from config import bot
from database.SQLOperator import SQLOperator
import keybords.kb as kb


class TransferStates(StatesGroup):
    send_points = State()
    load_pints = State()


async def transactions_start(message: types.Message):
    if message.from_user.id:
        chat_id = message.from_user.id
    else:
        chat_id = message.chat.id

    await bot.send_message(
        chat_id=chat_id,
        text='You can transfer 💰points for services to other members of the group!',
        reply_markup=await kb.one_button_inline_markup(
            text='Send points', callback='transfer_btn_send'))


async def transactions_send_points(call: types.CallbackQuery):
    await bot.send_message(
        text='Write massage format: *@username#amount* to send points to group member!',
        chat_id=call.message.chat.id,
        parse_mode=types.ParseMode.MARKDOWN)
    await TransferStates.send_points.set()


async def transactions_load_points(message: types.Message, state: FSMContext):
    async with state.proxy() as transfer:
        if message.text:
            try:
                user, amount = message.text.split('#')
            except ValueError:
                await state.finish()
                await bot.send_message(
                    text='Wrong format transfer massage! Try again /transfer.',
                    chat_id=message.chat.id)

            if not amount.isdigit():
                await state.finish()
                await bot.send_message(
                    chat_id=message.chat.id,
                    text='Incorrect format amount! Try again //transfer.')

            sender = await SQLOperator().async_get_referral(by_user_id=message.from_user.id)
            if sender:
                if sender['referral_balance'] >= int(amount):
                    recipient = await SQLOperator().async_select_user(username=user[1:] if user.startswith('@') else user)
                    if recipient:
                        if sender['referral_telegram_id'] != recipient['telegram_id']:
                            use_balance = await SQLOperator().async_get_referral(by_user_id=recipient['telegram_id'])
                            if not use_balance:
                                token = binascii.hexlify(os.urandom(4)).decode()
                                link = await _create_link(link_type='start', payload=token)
                                await SQLOperator().async_insert_into('reference_balance', (
                                    recipient['telegram_id'],
                                    link,
                                    0))

                            transfer['sender_telegram_id'] = sender['referral_telegram_id']
                            transfer['recipient_telegram_id'] = recipient['telegram_id']
                            transfer['amount'] = int(amount)
                            await TransferStates.next()

                            await bot.send_message(
                                text=f'Transaction: {message.from_user.username} -> *{amount}* -> {recipient["username"]}',
                                chat_id=message.chat.id,
                                parse_mode=types.ParseMode.MARKDOWN,
                                reply_markup=await kb.two_button_inline_markup(
                                    text=['Confirm', 'Refuse'],
                                    callback=['transfer_btn_confirm', 'transfer_btn_refuse']))
                        else:
                            await state.finish()
                            await bot.send_message(
                                chat_id=message.chat.id,
                                text="You can't send points to yourself!")
                    else:
                        await state.finish()
                        bot.send_message(
                            chat_id=message.chat.id,
                            text=f'User with username: {user} not in group.'
                        )
                else:
                    await state.finish()
                    await bot.send_message(
                        chat_id=message.chat.id,
                        text=f'You have no alot points. Balance: {sender["referral_balance"]} points.')


async def transaction_confirm_transfer(call: types.CallbackQuery, state: FSMContext):
    async with state.proxy() as transfer:
        await SQLOperator().async_transfer_points(_from=transfer['sender_telegram_id'],
                                                  to=transfer['recipient_telegram_id'],
                                                  amount=transfer['amount'])

        transaction = await SQLOperator().async_get_transaction(_from=transfer['sender_telegram_id'],
                                                                to=transfer['recipient_telegram_id'],
                                                                amount=transfer['amount'])
        recipient = await SQLOperator().async_select_user(user_id=transfer['recipient_telegram_id'])
        await bot.send_message(
            chat_id=call.message.chat.id,
            text='------------------------------\n'
                 f'  transaction: #000{transaction["id"]}\n'
                 f'------------------------------\n'
                 f'  from: {call.from_user.username}\n'
                 f'  to: {recipient["username"]} \n'
                 f'------------------------------\n'
                 f'\n'
                 f'                     ~{transfer["amount"]}\n'
                 f'------------------------------')
        await bot.send_message(
            chat_id=transfer['recipient_telegram_id'],
            text='------------------------------\n'
                 f'  transaction: #000{transaction["id"]}\n'
                 f'------------------------------\n'
                 f'  from: {call.from_user.username}\n'
                 f'  to: {recipient["username"]} \n'
                 f'------------------------------\n'
                 f'\n'
                 f'                     ~{transfer["amount"]}\n'
                 f'------------------------------')
        await state.finish()


async def transactions_refuse_transfer(call: types.CallbackQuery):
    await bot.send_message(
        chat_id=call.message.chat.id,
        text='Points transfer canceled!')


def register_transactions_handlers(dp: Dispatcher):
    dp.register_message_handler(transactions_start, commands=['transfer'])
    dp.register_callback_query_handler(transactions_start, lambda call: call.data == 'transactions_btn_transactions')
    dp.register_callback_query_handler(transactions_send_points, lambda call: call.data == 'transfer_btn_send')
    dp.register_message_handler(transactions_load_points, state=TransferStates.send_points)
    dp.register_callback_query_handler(transaction_confirm_transfer, lambda call: call.data == 'transfer_btn_confirm')
    dp.register_callback_query_handler(transactions_refuse_transfer, lambda call: call.data == 'transfer_btn_refuse')
