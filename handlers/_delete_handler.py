import sys, os


def create_new_python_file(file_name):
    if not file_name.endswith('.py'):
        file_name += '.py'

    if not os.path.exists(file_name):
        print(f'"{file_name}": is missing')
        return sys.exit(1)

    name = file_name.split('.')[0]

    agreement = input(f'Would you want do delete {file_name}: (no/yes) ')
    if agreement == 'no':
        return exit(0)
    if agreement == 'yes':
        os.remove(file_name)

        with open('../main.py') as main_file:
            code = main_file.readlines()
            for n, line in enumerate(code):
                if name in line.strip():
                    code[n] = line.strip().replace(', ' + name, '\n').replace(', ' + name + ',', '\n')

                if line.strip().startswith(name):
                    code[n] = ''

        with open('../main.py', 'w') as file:
            file.write(''.join(code))

        print(f'{name} delete from main.py')


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print("Usage: python _delete_handler.py <file_name>")
        sys.exit(1)

    file_name = sys.argv[1]
    create_new_python_file(file_name)
