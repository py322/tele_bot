from aiogram import types, Dispatcher
from config import bot
from aiogram.types import InlineKeyboardMarkup, InlineKeyboardButton
from database.SQLOperator import SQLOperator


answers = {}


async def question_action_questions1(call: types.CallbackQuery):
    button_call_1 = InlineKeyboardButton('Yes', callback_data='question_btn_yes')
    button_call_2 = InlineKeyboardButton('No', callback_data='question_btn_no')
    markup = InlineKeyboardMarkup()
    markup.add(button_call_1, button_call_2)
    await bot.send_message(text="Are you student?", chat_id=call.message.chat.id, reply_markup=markup)


async def answer_questions1_student(call: types.CallbackQuery):
    button_call_next = InlineKeyboardButton('next', callback_data='next_2')
    markup = InlineKeyboardMarkup()
    markup.add(button_call_next)
    answers['student'] = True
    await bot.send_message(text='Ok', chat_id=call.message.chat.id, reply_markup=markup)


async def answer_questions1_not_student(call: types.CallbackQuery):
    button_call_next = InlineKeyboardButton('next', callback_data='next_2')
    markup = InlineKeyboardMarkup()
    markup.add(button_call_next)
    answers['student'] = False
    await bot.send_message(text='Ok', chat_id=call.message.chat.id, reply_markup=markup)


async def question_action_questions2(call: types.CallbackQuery):
    button_call_1 = InlineKeyboardButton('Male', callback_data='question_btn_male')
    button_call_2 = InlineKeyboardButton('Female', callback_data='question_btn_female')
    markup = InlineKeyboardMarkup()
    markup.add(button_call_1, button_call_2)
    await bot.send_message(text='Male or Female?', chat_id=call.message.chat.id, reply_markup=markup)


async def answer_questions2_male(call: types.CallbackQuery):
    button_call_next = InlineKeyboardButton('next', callback_data='next_3')
    markup = InlineKeyboardMarkup()
    markup.add(button_call_next)
    answers['sex'] = 'male'
    await bot.send_message(text='Excellent', chat_id=call.message.chat.id, reply_markup=markup)


async def answer_questions2_female(call: types.CallbackQuery):
    button_call_next = InlineKeyboardButton('next', callback_data='next_3')
    markup = InlineKeyboardMarkup()
    markup.add(button_call_next)
    answers['sex'] = 'female'
    await bot.send_message(text='Ok', chat_id=call.message.chat.id, reply_markup=markup)


async def question_action_questions3(call: types.CallbackQuery):
    button_call_1 = InlineKeyboardButton('Java Script', callback_data='question_btn_java')
    button_call_2 = InlineKeyboardButton('Python', callback_data='question_btn_python')
    markup = InlineKeyboardMarkup()
    markup.add(button_call_1, button_call_2)
    await bot.send_message(text="What language you are coding?", chat_id=call.message.chat.id, reply_markup=markup)


async def answer_questions3_java(call: types.CallbackQuery):
    button_call_next = InlineKeyboardButton('next', callback_data='next_4')
    markup = InlineKeyboardMarkup()
    markup.add(button_call_next)
    answers['coding_language'] = 'java'
    await bot.send_message(text='Ok', chat_id=call.message.chat.id, reply_markup=markup)


async def answer_questions3_python(call: types.CallbackQuery):
    button_call_next = InlineKeyboardButton('next', callback_data='next_4')
    markup = InlineKeyboardMarkup()
    markup.add(button_call_next)
    answers['coding_language'] = 'python'
    await bot.send_message(text='Super', chat_id=call.message.chat.id, reply_markup=markup)


async def question_action_questions4(call: types.CallbackQuery):
    button_call_1 = InlineKeyboardButton('Frontend', callback_data='question_btn_front')
    button_call_2 = InlineKeyboardButton('Backend', callback_data='question_btn_back')
    markup = InlineKeyboardMarkup()
    markup.add(button_call_1, button_call_2)
    await bot.send_message(text="What's your specialization in GEEKS?", chat_id=call.message.chat.id, reply_markup=markup)


async def answer_questions4_front(call: types.CallbackQuery):
    button_call_next = InlineKeyboardButton('next', callback_data='next_5')
    markup = InlineKeyboardMarkup()
    markup.add(button_call_next)
    answers['specialization'] = 'frontend'
    await bot.send_message(text='Perfect', chat_id=call.message.chat.id, reply_markup=markup)


async def answer_questions4_back(call: types.CallbackQuery):
    button_call_next = InlineKeyboardButton('next', callback_data='next_5')
    markup = InlineKeyboardMarkup()
    markup.add(button_call_next)
    answers['specialization'] = 'backend'
    await bot.send_message(text='Ok', chat_id=call.message.chat.id, reply_markup=markup)


async def set_answers_to_db(call: types.CallbackQuery):
    if answers:
        await SQLOperator().async_insert_into('users_info', (
            answers['student'],
            answers['sex'],
            answers['coding_language'],
            answers['specialization'],
            call.from_user.id))

        await bot.send_message(chat_id=call.message.chat.id, text='Super!')
    else:
        await bot.send_message(chat_id=call.message.chat.id, text='Something went wrong!')


def register_questions_handlers(dp: Dispatcher):
    dp.register_callback_query_handler(question_action_questions1, lambda call: call.data == 'start_btn_yes')
    dp.register_callback_query_handler(answer_questions1_student, lambda call: call.data == 'question_btn_yes')
    dp.register_callback_query_handler(answer_questions1_not_student, lambda call: call.data == 'question_btn_no')
    dp.register_callback_query_handler(question_action_questions2, lambda call: call.data == 'next_2')
    dp.register_callback_query_handler(answer_questions2_female, lambda call: call.data == 'question_btn_female')
    dp.register_callback_query_handler(answer_questions2_male, lambda call: call.data == 'question_btn_male')
    dp.register_callback_query_handler(question_action_questions3, lambda call: call.data == 'next_3')
    dp.register_callback_query_handler(answer_questions3_java, lambda call: call.data == 'question_btn_java')
    dp.register_callback_query_handler(answer_questions3_python, lambda call: call.data == 'question_btn_python')
    dp.register_callback_query_handler(question_action_questions4, lambda call: call.data == 'next_4')
    dp.register_callback_query_handler(answer_questions4_back, lambda call: call.data == 'question_btn_back')
    dp.register_callback_query_handler(answer_questions4_front, lambda call: call.data == 'question_btn_front')
    dp.register_callback_query_handler(set_answers_to_db, lambda call: call.data == 'next_5')



