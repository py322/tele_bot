import sys, os


def create_new_python_file(file_name):
    if not file_name.endswith('.py'):
        file_name += '.py'

    if os.path.exists(file_name):
        print(f'"{file_name}": already exists!')
        return sys.exit(1)

    name = file_name.split('.')[0]

    imports = ['import asyncio',
               'from aiogram import types, Dispatcher',
               'from config import bot',
               'from database.SQLOperator import SQLOperator',
               'import keybords.kb as kb',]

    with open(file_name, 'w') as new_file:
        for imp in imports:
            new_file.write(f'{imp}\n')

        new_file.write('\n' * 10)
        new_file.write(f'def register_{name}_handlers(dp: Dispatcher):\n')
        new_file.write('    pass\n')

    print(f'New handler: {file_name}')

    with open('../main.py') as main_file:
        code = main_file.readlines()
        for n, line in enumerate(code):
            if line.strip().startswith('from handlers'):
                code[n] = line.strip() + ', ' + name + '\n'

            anchor = '# filter for message always at the END'
            if line.strip() == anchor:
                new_line = f"{name}.register_{name}_handlers(dp)\n" + anchor + '\n'
                code[n] = new_line

    with open('../main.py', 'w') as file:
        file.write(''.join(code))

    print(f'{name} import to main.py')


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print("Usage: python _get_new_file.py <file_name>")
        sys.exit(1)

    file_name = sys.argv[1]
    create_new_python_file(file_name)
