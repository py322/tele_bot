from aiogram import types, Dispatcher
from config import bot
from database.SQLOperator import SQLOperator
import keybords.kb as kb
from scraping.scraping_bybit import MyToken


async def start_command_p2p(message: types.Message):
    await bot.send_message(
        chat_id=message.chat.id,
        text=' You can take p2p list from Bybit',
        reply_markup=await kb.two_button_inline_markup(
            text=['USDT - RUB', 'USDT - KGS'],
            callback=['p2p_btn_rub', 'p2p_btn_kgs']))


async def scraper_coll_p2p_rub(coll: types.CallbackQuery):
    p2p_rub = MyToken().get_USDT_RUB()
    data = []
    if p2p_rub:
        for i in p2p_rub:
            data.append(
                f"👤{i['nickName']} buy {i['tokenId']} for <b>{i['price']}</b> {i['currencyId']} - "
                f"limit: {round(float(i['lastQuantity']), 3)}")

        data = '\n'.join(data)
        print(data)
        await bot.send_message(
            chat_id=coll.message.chat.id,
            text=data,
            parse_mode=types.ParseMode.HTML)


async def scraper_coll_p2p_kgs(coll: types.CallbackQuery):
    print('kgs')
    p2p_kgs = MyToken().get_USDT_KSG()
    data = []
    if p2p_kgs:
        for i in p2p_kgs:
            data.append(
                f"👤{i['nickName']} buy {i['tokenId']} for <b>{i['price']}</b> {i['currencyId']} - "
                f"limit: {round(float(i['lastQuantity']), 3)}")

        data = '\n'.join(data)
        print(data)
        await bot.send_message(
            chat_id=coll.message.chat.id,
            text=data,
            parse_mode=types.ParseMode.HTML)


def register_scraper_handlers(dp: Dispatcher):
    dp.register_message_handler(start_command_p2p, commands=['p2p'])
    dp.register_callback_query_handler(scraper_coll_p2p_rub, lambda call: call.data == 'p2p_btn_rub')
    dp.register_callback_query_handler(scraper_coll_p2p_kgs, lambda call: call.data == 'p2p_btn_kgs')
