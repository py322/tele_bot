import random
from aiogram import types, Dispatcher
from aiogram.dispatcher import FSMContext
from config import bot, group_id
from aiogram.dispatcher.filters.state import State, StatesGroup
from database.SQLOperator import SQLOperator
from keybords import kb


class ComplaintState(StatesGroup):
    complaint_message = State()
    load_report_member = State()
    send_message = State()


async def is_admin(user_id: int):
    member = await bot.get_chat_member(chat_id=group_id, user_id=user_id)
    return True if member['status'] in ['creator', 'admin'] else False


async def in_chat(user_id: int):
    member = await bot.get_chat_member(chat_id=group_id, user_id=user_id)
    return True if member['status'] not in ['kicked', 'left'] else False


async def start_command_report(message: types.Message):
    await message.reply(
        text="You can report a user whose behavior does not comply with the group's rules. You are ready?",
        reply_markup=await kb.two_button_inline_markup(
            text=['Sure', 'Not i"m not'],
            callback=['report_btn_yes', 'report_btn_no']))


async def command_report_no(call: types.CallbackQuery):
    await bot.send_message(
        chat_id=call.message.chat.id,
        text="It's OK. See you later.")


async def command_report_yes(call: types.CallbackQuery):
    await bot.send_message(
        chat_id=call.message.chat.id,
        text='Forward his message or write his nickname, first name or id...')
    await ComplaintState.complaint_message.set()


async def load_report_member(message: types.Message, state: FSMContext):
    async with state.proxy() as report:
        if message.forward_from:
            report['to_member_id'] = message.forward_from.id
            report['to_username'] = message.forward_from.username
            report['to_first_name'] = message.forward_from.first_name
        else:
            report['to_member'] = message.text

        if len(report) == 3:
            chat = await in_chat(user_id=report['to_member_id'])
            if not chat:
                await state.finish()
                await bot.send_message(
                    chat_id=message.chat.id,
                    text="I'm sorry, but the member is not in group")
            else:
                report['to_member'] = "I'll be back!"

        elif len(report) == 1:
            if report['to_member'].isdigit():
                chat = await in_chat(user_id=report['to_member'])
                if not chat:
                    await bot.send_message(
                        chat_id=message.chat.id,
                        text="I'm sorry, but the member with this ID not in group, Try with USERNAME.")
                    await state.finish()
                    await start_command_report(message)
                else:
                    user = await SQLOperator().async_select_user(user_id=report['to_member'])
                    if user:
                        report['to_member_id'] = user['telegram_id']
                        report['to_username'] = user['username']
                        report['to_first_name'] = user['first_name']
            else:
                user_for_username = await SQLOperator().async_select_user(username=report['to_member'].replace('@', ''))
                if user_for_username:
                    report['to_member_id'] = user_for_username['telegram_id']
                    report['to_username'] = user_for_username['username']
                    report['to_first_name'] = user_for_username['first_name']

                user_for_first_name = await SQLOperator().async_select_user(first_name=report['to_member'])
                if user_for_first_name and len(user_for_first_name) == 1:
                    report['to_member_id'] = user_for_first_name['telegram_id']
                    report['to_username'] = user_for_first_name['username']
                    report['to_first_name'] = user_for_first_name['first_name']

        else:
            await state.finish()
            await bot.send_message(
                chat_id=message.chat.id,
                text="I'm sorry, but I did not found this user :/")

        if not report:
            await state.finish()
            await bot.send_message(
                chat_id=message.chat.id,
                text='Something went wrong, try again later...')

        elif len(report) < 4:
            await state.finish()
            await bot.send_message(
                chat_id=message.chat.id,
                text=f"I'm sorry, I did not found users: {report['to_member']}. "
                     f"Try with ID.")

        elif len(report) == 4:
            await SQLOperator().async_update_report(_from=message.from_user.id, to=report['to_member_id'])
            await bot.send_message(
                chat_id=message.chat.id,
                text=f'{report["to_username"] if report["to_username"] else "To the user"} will be subject '
                     f'to sanctions in accordance with the rules of the group. '
                     'Thank you for your feedback!')

            ru = await SQLOperator().async_select_user(user_id=message.from_user.id)
            report['from_name'] = ru['username'] if ru['username'] else ru['first_name']
            report['from_member_id'] = message.from_user.id

            await bot.send_message(
                chat_id=report['to_member_id'],
                text='Your post received a user complaint. '
                     'Please follow the group rules otherwise you will be banned.',
                reply_markup=await kb.two_button_inline_markup(
                    text=['opportunity', 'miss'],
                    callback=['report_btn_appeals', 'report_btn_not_appeals']))
            await ComplaintState.next()


async def report_appeals(call: types.CallbackQuery, state: FSMContext):
    async with state.proxy() as report:
        random_users = ['Boloto89', 'AzAzello', '~Voland~', "i'm_groot", 'Shcoding', 'Valentin', 'ADALET', 'Geolog']
        report_user = report['from_name']
        options = [report_user] + random.sample(random_users, 3)
        random.shuffle(options)
        index = options.index(report_user)
        report['current_answer'] = index

        await bot.send_message(
            chat_id=report['to_member_id'],
            text='Who send report to you?',
            reply_markup=await kb.four_button_inline_markup(
                text=options,
                callback=['answer_0', 'answer_1', 'answer_2', 'answer_3']))
    await ComplaintState.next()


async def appeals_report_answer(call: types.CallbackQuery, state: FSMContext):
    async with state.proxy() as report:
        current_answer = report['current_answer']
        user_answer = int(call.data.split('_')[-1])

        if current_answer == user_answer:
            await SQLOperator().async_update_report(_from=report['from_member_id'], to=report['to_member_id'], summa=False)
            await bot.send_message(
                chat_id=call.message.chat.id,
                text="You're right, but I didn't tell you that...")
        else:
            await bot.send_message(
                chat_id=call.message.chat.id,
                text="You won't guess. You shouldn't have thought badly of him...")
    await state.finish()


async def report_not_appeals(call: types.CallbackQuery, state: FSMContext):
    async with state.proxy() as report:
        ban_user = await SQLOperator().async_get_count_reports(_from=call.from_user.id, to=report['to_member_id'])
        if ban_user:
            if ban_user['count'] % 3 == 0:
                await bot.send_message(
                    chat_id=report['to_member_id'],
                    text='According to the chat rules, you are going to be banned!'
                         ' For appeals, please contact the group admin.')
    await state.finish()


def register_complaint_handlers(dp: Dispatcher):
    dp.register_message_handler(start_command_report, commands=['report'])
    dp.register_callback_query_handler(command_report_no, lambda call: call.data == 'report_btn_no')
    dp.register_callback_query_handler(command_report_yes, lambda call: call.data == 'report_btn_yes')
    dp.register_message_handler(load_report_member, state=ComplaintState.complaint_message)
    dp.register_callback_query_handler(report_appeals, lambda call: call.data == 'report_btn_appeals',
                                       state=ComplaintState.load_report_member)
    dp.register_callback_query_handler(report_not_appeals, lambda call: call.data == 'report_btn_not_appeals',
                                       state=ComplaintState.load_report_member)
    dp.register_callback_query_handler(appeals_report_answer, lambda call: 'answer_' in call.data,
                                       state=ComplaintState.send_message)
