from aiogram import types, Dispatcher
from config import bot, group_id
from database.SQLOperator import SQLOperator
import keybords.kb as kb


async def is_admin(chat_id: types.Message, user_id: types.Message):
    member = await bot.get_chat_member(chat_id=chat_id, user_id=user_id)
    return True if member['status'] in ['creator', 'admin'] else False


async def admin_action_echo_ban(message: types.Message):
    words = ["damn", "fuck", 'bitch']
    if message.chat.id == group_id:
        for ban_word in words:
            if ban_word in message.text.lower().strip():
                await bot.delete_message(
                    chat_id=message.chat.id,
                    message_id=message.message_id)
                await bot.send_message(
                    chat_id=message.chat.id,
                    text=f"{message.from_user.username}: "
                         f"{await SQLOperator().async_send_preban_massage(user_id=message.from_user.id)}")

                is_baned = await SQLOperator.async_is_baned_user(user_id=message.from_user.id)
                if is_baned:
                    admin = await is_admin(chat_id=message.chat.id, user_id=message.from_user.id)
                    if admin:
                        await SQLOperator().async_update_ban_user(message.from_user.id, (1, 1, message.from_user.id))
                    else:
                        print('User baned')
                        #     current_date = datetime.now()
                        #     await bot.ban_chat_member(
                        #         chat_id=message.chat.id,
                        #         user_id=message.from_user.id,
                        #         until_date= current_date + timedelta(minutes=30))
                else:
                    await SQLOperator().async_update_ban_user(message.from_user.id, (1, 0, message.from_user.id))


async def admin_command_ban(message: types.Message):
    member = await bot.get_chat_member(chat_id=message.chat.id, user_id=message.from_user.id)
    if member['status'] in ['creator', 'admin']:
        
        await bot.send_message(
            text='Take option',
            chat_id=message.from_user.id,
            reply_markup=await kb.two_button_inline_markup(
                text=['all users', 'ban users'],
                callback=['admin_btn_all_users', 'admin_btn_ban_users']))


async def admin_action_all_users(call: types.CallbackQuery):
    if is_admin(chat_id=call.message.chat.id, user_id=call.from_user.id):
        users = await SQLOperator().async_select_all_users()
        data = []
        for num, user in enumerate(users, 1):
            if not user["username"]:
                data.append(f'{num} - [{user["first_name"]}](tg://user?id={user["telegram_id"]})')
            else:
                data.append(f'{num} - [{user["username"]}](tg://user?id={user["telegram_id"]})')

        data = "\n".join(data)

        await bot.send_message(
            text=data,
            chat_id=call.message.chat.id,
            parse_mode=types.ParseMode.MARKDOWN,)


async def admin_action_ban_users(call: types.CallbackQuery):
    if is_admin(chat_id=call.message.chat.id, user_id=call.from_user.id):
        ban_users = await SQLOperator().async_select_all_ban_users()
        data = []
        for user in ban_users:
            if user['count'] > 0:
                if not user["nickname"]:
                    data.append(f'{user["id"]} - [{user["first_name"]}](tg://user?id={user["telegram_id"]}):'
                                f' {ban_users["count"]}')
                else:
                    data.append(f'{user["id"]} - [{user["nickname"]}](tg://user?id={user["telegram_id"]}):'
                                f' {user["count"]}')
        data = "\n".join(data)

        await bot.send_message(
            text=data,
            chat_id=call.message.chat.id,
            parse_mode=types.ParseMode.MARKDOWN,
            reply_markup=await kb.one_button_inline_markup(
                text='Send message for all in ban list',
                callback='admin_btn_send_preban_message'))


async def admin_action_send_preban_message(call: types.CallbackQuery):
    ban_users = await SQLOperator().async_select_all_ban_users()
    for user in ban_users:
        text = f'Dear {user["nickname"] if user["nickname"] else "friend"}.' \
               f'You have {user["count"]} group rule violations.' \
               f'After 3 violation, you will be baned for 30 min. Be careful!'

        await bot.send_message(text=text, chat_id=user['telegram_id'])


def register_admin_actions_handlers(dp: Dispatcher):
    dp.register_message_handler(admin_command_ban, commands=['ban'])
    dp.register_callback_query_handler(admin_action_ban_users, lambda call: call.data == 'admin_btn_ban_users')
    dp.register_callback_query_handler(admin_action_all_users, lambda call: call.data == 'admin_btn_all_users')
    dp.register_callback_query_handler(admin_action_send_preban_message,
                                       lambda call: call.data == 'admin_btn_send_preban_message')
    dp.register_message_handler(admin_action_echo_ban)
