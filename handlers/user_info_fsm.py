from aiogram import types, Dispatcher
from aiogram.dispatcher import FSMContext
from config import bot
from database.SQLOperator import SQLOperator
from aiogram.dispatcher.filters.state import State, StatesGroup
from aiogram.types import ContentType
import keybords.kb as kb


class UserInfoState(StatesGroup):
    yo = State()
    sportsmen = State()
    summer_sport = State()
    winter_sport = State()
    sport_photo = State()
    save = State()


async def user_info_fsm_start(message: types.Message):
    await bot.send_message(
        chat_id=message.chat.id,
        text='How old are you?')
    await UserInfoState.yo.set()


async def user_info_fsm_sportsmen(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        if message.text.isdigit():
            data['yo'] = message.text
        else:
            await state.finish()
            await bot.send_message(
                chat_id=message.chat.id,
                text='Use only numeric! Try again /info')

    await UserInfoState.next()
    await message.answer(
        text='Are you do spotr?')


async def user_info_fsm_summer_sport(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        data['sportsmen'] = message.text
    await UserInfoState.next()
    await message.answer(
        text='What your favorite summer sport?')


async def user_info_fsm_winter_sport(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        data['summer_sport'] = message.text
    await UserInfoState.next()
    await message.answer(
        text='What your favorite winter sport?')


async def user_info_fsm_sport_photo(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        data['winter_sport'] = message.text
    await UserInfoState.next()
    await message.answer(
        text='Send me your sport photo, plz')


async def load_user_info_fsm_sport_photo(message: types.Message, state: FSMContext):
    path = await message.photo[-1].download(destination_dir=r"E:\MY\telegramm_bot\media")
    async with state.proxy() as data:
        await message.answer("Wow, good photo!")
        data['photo'] = path.name
        if data:
            with open(data['photo'], "rb") as photo:
                await bot.send_photo(
                    chat_id=message.chat.id,
                    photo=photo,
                    caption=f"*Years old:* {data['yo']}\n"
                            f"*Is sportsman:* {data['sportsmen']}\n"
                            f"*Summer sport:* {data['summer_sport']}\n"
                            f"*Winter sport:* {data['winter_sport']}\n",
                    parse_mode=types.ParseMode.MARKDOWN,
                    reply_markup=await kb.two_button_inline_markup(
                        text=["Yes, it's all right", 'I have a mistake'],
                        callback=['user_info_btn_save', 'user_info_btn_replay']))

                await UserInfoState.next()
        else:
            await state.finish()
            await bot.send_message(
                chat_id=message.chat.id,
                text='Something goes wrong:/')


async def user_info_save(call: types.CallbackQuery, state: FSMContext):
    async with state.proxy() as data:
        await SQLOperator().async_insert_into('sport_users', (
            data['yo'],
            data['sportsmen'],
            data['summer_sport'],
            data['winter_sport'],
            data['photo'],
            call.from_user.id))
    await state.finish()

    await bot.send_message(
        chat_id=call.message.chat.id,
        text='Your info saved!')


async def user_info_fsm_restart(call: types.CallbackQuery, state: FSMContext):
    await state.finish()
    await bot.send_message(
        chat_id=call.message.chat.id,
        text='How old are you?')
    await UserInfoState.yo.set()


def register_user_info_fsm_handlers(dp: Dispatcher):
    dp.register_message_handler(user_info_fsm_start, commands=['info'])
    dp.register_message_handler(user_info_fsm_sportsmen, content_types=["text"], state=UserInfoState.yo)
    dp.register_message_handler(user_info_fsm_summer_sport, content_types=["text"], state=UserInfoState.sportsmen)
    dp.register_message_handler(user_info_fsm_winter_sport, content_types=["text"], state=UserInfoState.summer_sport)
    dp.register_message_handler(user_info_fsm_sport_photo, content_types=["text"], state=UserInfoState.winter_sport)
    dp.register_message_handler(load_user_info_fsm_sport_photo,
                                content_types=ContentType.PHOTO,
                                state=UserInfoState.sport_photo)
    dp.register_callback_query_handler(user_info_save,
                                       lambda call: call.data == 'user_info_btn_save',
                                       state=UserInfoState.save)
    dp.register_callback_query_handler(user_info_fsm_restart,
                                       lambda call: call.data == 'user_info_btn_replay',
                                       state=UserInfoState.save)
