import asyncio
from aiogram import types, Dispatcher
from config import bot
from database.SQLOperator import SQLOperator
import keybords.kb as kb
from scraping.tproger_scraper import ITProger


async def start_command_articles(message: types.Message):
    if message.from_user.id: chat_id = message.from_user.id
    else: chat_id = message.chat.id

    await bot.send_message(
        chat_id=chat_id,
        text='You can read new articles or favorite',
        reply_markup=await kb.two_button_inline_markup(
            text=['Read articles', 'Get favorite'],
            callback=['articles_btn_new', 'article_btn_favorite']))


async def articles_read_articles(call: types.CallbackQuery):
    articles = ITProger().parse_data()
    for article in articles:
        await SQLOperator().async_insert_into('articles', (
            article['date'],
            article['title'],
            article['link'],
            article['post_data'],
            0))

    download_articles = await SQLOperator().async_get_last_articles()
    for article in download_articles:
        print(article)
        text = f'<b><a href="{article["link"]}">{article["title"]}</a></b>\n'\
               f"<i>{article['date']}</i>\n" \
               f"👍: <u>{article['likes']}</u>"

        await bot.send_message(
            chat_id=call.message.chat.id,
            text=text,
            parse_mode=types.ParseMode.HTML,
            reply_markup=await kb.one_button_inline_markup(
                text='Save to favorite!',
                callback=f'articles_btn_{article["id"]}'))


async def articles_favorite_articles(call: types.CallbackQuery):
    favorite_list = await SQLOperator().async_select_favorite_articles(user_id=call.from_user.id)
    print(favorite_list)
    await bot.send_message(
        chat_id=call.message.chat.id,
        text='What articles you want to delete from favorite?',
        reply_markup=await kb.delete_articles_inline_markup(favorite_list))


async def articles_save_to_favorite(call: types.CallbackQuery):
    article_id = call.data.split('_')[-1]
    article = await SQLOperator().async_select_article(article_id=article_id)

    await SQLOperator().async_insert_into('favorite_articles', (
        call.from_user.id,
        article_id))
    await SQLOperator().async_update_article_likes(article_id=article_id)
    await bot.send_message(
        chat_id=call.message.chat.id,
        text=f'Article: *{article["title"]}* add to favorite successfully!',
        parse_mode=types.ParseMode.MARKDOWN)


async def articles_delete_articles(call: types.CallbackQuery):
    article_id = call.data.split('_')[-1]
    article = await SQLOperator().async_select_article(article_id=article_id)
    await SQLOperator().async_delete_article_from_favorite(user_id=call.from_user.id, article_id=article_id)

    favorite_list = await SQLOperator().async_select_favorite_articles(user_id=call.from_user.id)
    await bot.delete_message(
        chat_id=call.message.chat.id,
        message_id=call.message.message_id)
    await bot.send_message(
        chat_id=call.message.chat.id,
        text=f'Article: *{article["title"]}* delete successfully! You want to delete anything else?',
        reply_markup=await kb.delete_articles_inline_markup(favorite_list),
        parse_mode=types.ParseMode.MARKDOWN)


def register_articles_handlers(dp: Dispatcher):
    dp.register_message_handler(start_command_articles, commands=['articles'])
    dp.register_callback_query_handler(start_command_articles, lambda call: 'start_btn_articles' == call.data)
    dp.register_callback_query_handler(articles_read_articles, lambda call: 'articles_btn_new' == call.data)
    dp.register_callback_query_handler(articles_favorite_articles, lambda call: 'article_btn_favorite' == call.data)
    dp.register_callback_query_handler(articles_save_to_favorite, lambda call: 'articles_btn_' in call.data)
    dp.register_callback_query_handler(articles_delete_articles, lambda call: 'delete_article_btn_' in call.data)
