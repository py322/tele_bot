from aiogram.utils import executor
from config import dp
from handlers import start, questions, admin_actions, user_info_fsm, complaint, reference, transactions, scraper, articles
from database.SQLOperator import SQLOperator
import database.sql_queries as sql


async def on_startup(x):
    tables = {
        'users': sql.users_headers,
        'users_info': sql.users_info,
        'ban_users': sql.ban_users_headers,
        'sport_users': sql.sport_users_headers,
        'report_users': sql.report_users_headers,
        'reference_balance': sql.reference_balance_headers,
        'referrals': sql.referrals_headers,
        'transactions': sql.transactions_headers,
        'articles': sql.articles_headers,
        'favorite_articles': sql.favorite_articles_headers,
    }

    for name, header in tables.items():
        await SQLOperator().async_create_table(name, header)


start.register_start_handlers(dp)
questions.register_questions_handlers(dp)
user_info_fsm.register_user_info_fsm_handlers(dp)
complaint.register_complaint_handlers(dp)
reference.register_reference_handlers(dp)
transactions.register_transactions_handlers(dp)
scraper.register_scraper_handlers(dp)
articles.register_articles_handlers(dp)
# filter for message always at the END
admin_actions.register_admin_actions_handlers(dp)


if __name__ == '__main__':
    executor.start_polling(dp, skip_updates=True, on_startup=on_startup)
