users_headers = (
    'id INTEGER',
    'telegram_id INTEGER PRIMARY KEY',
    'first_name STRING',
    'second_name STRING',
    'username STRING',)


users_info = (
    'id INTEGER PRIMARY KEY AUTOINCREMENT',
    'student BOOL',
    'sex STRING',
    'coding_language STRING',
    'specialization STRING,'
    'telegram_user_id INTEGER, FOREIGN KEY (telegram_user_id) REFERENCES users(telegram_id)'
)

ban_users_headers = (
    'id INTEGER PRIMARY KEY AUTOINCREMENT',
    'count INTEGER DEFAULT 0',
    'is_admin BOOL DEFAULT 0',
    'telegram_user_id INTEGER, FOREIGN KEY (telegram_user_id) REFERENCES users(telegram_id)'
)

sport_users_headers = (
    'id INTEGER PRIMARY KEY AUTOINCREMENT',
    'yo INTEGER',
    'sportsmen STRING',
    'summer_sport STRING',
    'winter_sport STRING',
    'photo_path STRING,'
    'telegram_user_id INTEGER UNIQUE, FOREIGN KEY (telegram_user_id) REFERENCES users(telegram_id)'
)

report_users_headers = (
    'id INTEGER PRIMARY KEY AUTOINCREMENT',
    'reported_user_telegram_id INTEGER',
    'reported_user_name STRING',
    'reason TEXT',
    'count INTEGER',
    'report_user_telegram_id INTEGER, FOREIGN KEY (report_user_telegram_id) REFERENCES users(telegram_id)',
    'UNIQUE (reported_user_telegram_id, report_user_telegram_id)'
)

reference_balance_headers = (
    'id INTEGER PRIMARY KEY AUTOINCREMENT',
    'referral_telegram_id INTEGER UNIQUE',
    'reference_link VARCHAR(50) UNIQUE',
    'referral_balance INTEGER, '
    'FOREIGN KEY (referral_telegram_id) REFERENCES users(telegram_id)'
)

referrals_headers = (
    'id INTEGER PRIMARY KEY AUTOINCREMENT',
    'referral_telegram_id INTEGER',
    'referable_telegram_id INTEGER',
    'UNIQUE (referral_telegram_id, referable_telegram_id),'
    'FOREIGN KEY (referral_telegram_id) REFERENCES reference_balance(referral_telegram_id)'
)

transactions_headers = (
    'id INTEGER PRIMARY KEY AUTOINCREMENT',
    'sender_telegram_id INTEGER',
    'recipient_telegram_id INTEGER',
    'amount INTEGER, FOREIGN KEY (sender_telegram_id) REFERENCES users(telegram_id)'
)

articles_headers = (
    'id INTEGER PRIMARY KEY AUTOINCREMENT',
    'date DATE',
    'title TEXT',
    'link VARCHAR(70)',
    'post_data STRING UNIQUE',
    'likes INTEGER DEFAULT 0'
)

favorite_articles_headers = (
    'id INTEGER PRIMARY KEY AUTOINCREMENT',
    'telegram_id INTEGER',
    'articles_id INTEGER UNIQUE, FOREIGN KEY (articles_id) REFERENCES articles(id)'
)
