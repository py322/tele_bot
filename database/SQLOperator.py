import asyncio
from sqlite3 import Error
import sqlite3

from config import db
from colorama import Fore

class SQLOperator:
    def __init__(self):
        self.__con = None
        self.__cur = None
        self.loop = asyncio.get_running_loop()
        self.__connect_db()

    def __connect_db(self):
        try:
            self.__con = sqlite3.connect(db, check_same_thread=False)
            self.__cur = self.__con.cursor()
            print(f'{Fore.GREEN}database({db}): connect successfully{Fore.RESET}')
        except Error as e:
            print(e)

    def create_table(self, name, *args):
        table = f'''CREATE TABLE IF NOT EXISTS {name}({', '.join(args[0])})'''
        if name and args:
            try:
                self.__cur.execute(table)
                self.__con.commit()

                # print(f'Table({name}) create successfully!')
            except Error as e:
                print(e)
        else:
            print('Not arguments!')

    async def async_create_table(self, name, *args) -> None:
        await self.loop.run_in_executor(None, self.create_table, name, *args)

    def _get_headers(self, table_name):
        headers = []
        info = []
        try:
            info = self.__cur.execute(f'PRAGMA table_info({table_name})').fetchall()
        except Error as e:
            print('_get_headers: ', e)
        for name_column in info: headers.append(name_column[1])

        return headers

    def insert_into(self, name: str, values: tuple):
        headers = self._get_headers(name)
        if 'id' in headers[0]:
            try:
                self.__cur.execute(
                    f'''INSERT OR IGNORE INTO {name} VALUES (NULL, {(", ".join(['?'] * (len(headers) - 1)))});''',
                    values)
            except Error as e:
                print(e)

        else:
            try:
                self.__cur.execute(f'''INSERT OR IGNORE INTO {name} VALUES ({(", ".join(['?'] * len(headers)))});''',
                                   values)
            except Error as e:
                print(e)

        self.__con.commit()

    async def async_insert_into(self, name, value):
        await self.loop.run_in_executor(None, self.insert_into, name, value)

    def _select_from_ban_users(self, user_id):
        result = []
        try:
            self.__cur.execute(f"SELECT * FROM ban_users WHERE telegram_user_id = '{user_id}';")
            result = self.__cur.fetchall()
        except Error as e:
            print(e)
        user_ban = {}
        if result:
            headers = self._get_headers('ban_users')
            user_ban = dict(zip(headers, result[0]))

        return user_ban

    def select_all_users(self):
        select = []
        users = []
        headers = self._get_headers('users')
        try:
            self.__cur.execute(f"SELECT * FROM users;")
            select = self.__cur.fetchall()
        except Error as e:
            print(e)

        for user in select: users.append(dict(zip(headers, user)))
        return users

    async def async_select_all_users(self):
        return await self.loop.run_in_executor(None, self.select_all_users)

    def select_user(self, user_id=None, username=None, first_name=None):
        result = []
        headers = self._get_headers('users')
        if user_id:
            try:
                self.__cur.execute(f"SELECT * FROM users WHERE telegram_id='{user_id}';")
                result = self.__cur.fetchall()
            except Error as e: print('SELECT user for id:', e)
        if username:
            try:
                self.__cur.execute(f"SELECT * FROM users WHERE username='{str(username)}';")
                result = self.__cur.fetchall()
            except Error as e: print('SELECT user for username:', e)
        if first_name:
            try:
                self.__cur.execute(f"SELECT * FROM users WHERE first_name='{str(first_name)}';")
                result = self.__cur.fetchall()
            except Error as e: print('SELECT user for first name:', e)

        return dict(zip(headers, result[0])) if result else {}

    async def async_select_user(self, user_id=None, username=None, first_name=None) -> dict:
        return await self.loop.run_in_executor(None, self.select_user, user_id, username, first_name)

    def select_all_ban_users(self):
        users = []
        try:
            self.__cur.execute(f'''SELECT bu.id, bu.count, bu.is_admin, u.username, u.first_name, u.telegram_id
                                   FROM ban_users AS bu LEFT JOIN users AS u ON bu.telegram_user_id = u.telegram_id;''')
            users = self.__cur.fetchall()
        except Error as e:
            print(e)

        headers = ['id', 'count', 'is_admin', 'nickname', 'first_name', 'telegram_id']

        return [dict(zip(headers, user)) for user in users] if users else []

    async def async_select_all_ban_users(self) -> list:
        return await self.loop.run_in_executor(None, self.select_all_ban_users)

    def is_ban_user(self, user_id: int):
        user_ban = self._select_from_ban_users(user_id)
        if user_ban:
            return user_ban['count'] >= 2 and True or False
        else:
            return False

    async def async_is_baned_user(self, user_id):
        return await self.loop.run_in_executor(None, self.is_ban_user, user_id)

    def update_ban_user(self, user_id, values):
        user_ban = self._select_from_ban_users(user_id)
        if not user_ban:
            try:
                self.__cur.execute(f'''INSERT OR IGNORE INTO ban_users VALUES (NULL, ?, ?, ?);''', values)
            except Error as e:
                print(e, 'insert')

        else:
            count = user_ban['count']
            try:
                self.__cur.execute(f'''UPDATE ban_users SET count={count + 1 if count < 2 else 0}
                                       WHERE telegram_user_id='{user_id}';''')
            except Error as e:
                print(e, 'update')

        self.__con.commit()

    async def async_update_ban_user(self, user_id, values):
        await self.loop.run_in_executor(self.update_ban_user, user_id, values)

    def send_preban_message(self, user_id):
        user_ban = self._select_from_ban_users(user_id)

        if user_ban:
            if user_ban['count'] == 0: return 'Первое предупреждение!'
            if user_ban['count'] == 1: return 'Последнее китайское предупреждение!'
            else: return 'Бан чата на пол часа!!!'
        else:
            return 'Первое предупреждение!'

    async def async_send_preban_massage(self, user_id):
        return await self.loop.run_in_executor(None, self.send_preban_message, user_id)

    def _if_user_in_report_users(self, report_user: int, reported_user: int):
        result = []
        headers = self._get_headers('report_users')
        try:
            self.__cur.execute(f'''SELECT * FROM report_users WHERE report_user_telegram_id='{report_user}' AND
                               reported_user_telegram_id='{reported_user}';''')
            result = self.__cur.fetchall()
        except Error:print('if_user_in_report_users -> None')

        return dict(zip(headers, result[0])) if result else {}

    def update_report(self, _from: int, to: int, reason='abuse', summa=True):
        user = self._if_user_in_report_users(_from, to)
        print(user)
        if not user:
            try:
                self.__cur.execute('''INSERT OR IGNORE INTO report_users VALUES (NULL, ?, ?, ?, ?);''', (
                    to,
                    reason,
                    1,
                    _from))
            except Error as e: print('INSERT INTO report_users:', e)
        else:
            count = user['count']
            if summa:
                try:
                    self.__cur.execute(f'''UPDATE report_users SET count={count + 1};''')
                except Error as e:
                    print('UPDATE report count+:', e)
            else:
                try:
                    self.__cur.execute(f'''UPDATE report_users SET count={count - 1};''')
                except Error as e: print('UPDATE report count-:', e)

        self.__con.commit()

    async def async_update_report(self, _from, to, reason='abuse', summa=True):
        await self.loop.run_in_executor(None, self.update_report, _from, to, reason, summa)

    def get_count_report(self, _from: int, to: int):
        user = self._if_user_in_report_users(_from, to)
        return user if user else {}

    async def async_get_count_reports(self, _from, to) -> dict:
        return await self.loop.run_in_executor(None, self.get_count_report, _from, to)

    def get_referral(self, by_user_id=None, by_link=None):
        headers = self._get_headers('reference_balance')
        result = []
        if by_user_id:
            try:
                self.__cur.execute(f'''SELECT * FROM reference_balance WHERE referral_telegram_id={by_user_id};''')
                result = self.__cur.fetchall()
            except Error as e: print('get_referral_by_user_id:', e)
        if by_link:
            try:
                self.__cur.execute(f'''SELECT * FROM reference_balance WHERE reference_link={by_link};''')
                result = self.__cur.fetchall()
            except Error as e: print('get_referral_by_link:', e)

        return dict(zip(headers, result[0])) if result else {}

    async def async_get_referral(self, by_user_id=None, by_link=None) -> dict:
        return await self.loop.run_in_executor(None, self.get_referral, by_user_id, by_link)

    def set_referral_balance(self, user_id: int):
        balance = []
        try:
            balance = self.__cur.execute(f'''SELECT FROM reference_balance SET balance 
                                            WHERE referral_telegram_id={user_id};''')
        except Error as e: print('select reference_balance:', e)
        balance = int(balance[0]) if balance else 0
        try:
            self.__cur.execute(f'''UPDATE reference_balance SET {balance + 100} WHERE 
                                referral_telegram_id={user_id};''')
        except Error as e: print('update balance:', e)
        self.__con.commit()

    async def async_set_referral_balance(self, user_id):
        await self.loop.run_in_executor(None, self.set_referral_balance, user_id)

    def references_in_pair(self, _from: int, to: int):
        pair = []
        try:
            pair = self.__cur.execute(f'''SELECT * FROM referrals WHERE referral_telegram_id={_from} 
                                      AND referable_telegram_id={to};''')
        except Error as e: print('get reference pair:', e)

        return pair and True or False

    async def async_references_in_pair(self, _from, to):
        await self.loop.run_in_executor(None, self.references_in_pair, _from, to)

    def set_reference_pair(self, _from: int, to: int):
        try:
            self.__cur.execute(f"""INSERT OR IGNORE INTO referrals VALUES(NULL, {_from}, {to});""")
        except Error as e: print('set reference pair:', e)
        self.__con.commit()

    def get_reference_list(self, user_id: int):
        headers = ['referable_id', 'username']
        result = []
        reference_list = []
        try:
            result = self.__cur.execute(f"""SELECT r.referable_telegram_id, u.username FROM referrals AS r 
                                        LEFT JOIN users AS u ON r.referable_telegram_id = u.telegram_id
                                        WHERE r.referral_telegram_id={user_id};""")
        except Error as e: print('get reference list:', e)

        if result:
            for row in result:
                reference_list.append(dict(zip(headers, row)))
        return reference_list

    async def async_get_reference_list(self, user_id) -> list:
        return await self.loop.run_in_executor(None, self.get_reference_list, user_id)

    async def async_set_reference_pair(self, _from, to):
        await self.loop.run_in_executor(None, self.set_reference_pair, _from, to)

    def transfer_points(self, _from: int, to: int, amount: int):
        balance = 0
        try:
            balance = self.__cur.execute(f'''SELECT FROM reference_balance SET balance 
                                         WHERE referral_telegram_id={_from};''')
        except Error as e: print('get balance from:', e)
        balance = int(balance[0])
        try:
            self.__cur.execute(f'''UPDATE reference_balance SET {balance - amount} WHERE 
                               referral_telegram_id={_from};''')
        except Error as e: print('transfer update balance:', e)
        balance = 0
        try:
            balance = self.__cur.execute(f'''SELECT FROM reference_balance SET balance 
                                         WHERE referral_telegram_id={to};''')
        except Error as e: print('get balance to:', e)
        try:
            self.__cur.execute(f'''UPDATE reference_balance SET {balance + amount} WHERE 
                                referral_telegram_id={to};''')
        except Error as e: print('transfer update balance:', e)
        try:
            self.__cur.execute(f'''INSERT INTO transactions VALUES(NULL, ?, ?, ?)''', (
                _from, to, amount))
        except Error as e: print('insert into transactions:', e)
        self.__con.commit()

    async def async_transfer_points(self, _from, to, amount):
        await self.loop.run_in_executor(None, self.transfer_points, _from, to, amount)

    def get_transaction(self,  _from: int, to: int, amount: int):
        headers = self._get_headers('transactions')
        transfer = []
        try:
            self.__cur.execute(f'''SELECT * FROM transactions WHERE  sender_telegram_id={_from}, 
            recipient_telegram_id={to} AND  amount={amount};''')
            transfer = self.__cur.fetchall()
        except Error as e: print('get transfer:', e)

        return dict(zip(headers, transfer)) if transfer else {}

    async def async_get_transaction(self, _from, to, amount) -> dict:
        return await self.loop.run_in_executor(None, self.get_transaction, _from, to, amount)

    def select_article(self, article_id) -> dict:
        headers = self._get_headers('articles')
        result = []
        try:
            self.__cur.execute(f'''SELECT * FROM articles WHERE id={article_id};''')
            result = self.__cur.fetchall()
        except Error as e: print('select articles:', e)

        return dict(zip(headers, result[0])) if result else {}

    async def async_select_article(self, article_id) -> dict:
        return await self.loop.run_in_executor(None, self.select_article, article_id)

    def get_last_articles(self) -> list:
        headers = self._get_headers('articles')
        result = []
        last_articles = []
        try:
            self.__cur.execute(f'''SELECT * FROM articles;''')
            result = self.__cur.fetchmany(5)
        except Error as e: print('select last articles:', e)

        if result:
            for row in result:
                last_articles.append(dict(zip(headers, row)))

        return last_articles

    async def async_get_last_articles(self) -> list:
        return await self.loop.run_in_executor(None, self.get_last_articles)

    def update_article_likes(self, article_id) -> None:
        article = self.select_article(article_id)
        print(article)
        if article:
            likes = int(article['likes'])
            try:
                self.__cur.execute(f'''UPDATE articles SET likes={likes + 1} WHERE id={article_id};''')
            except Error as e:
                print('update article likes:', e)
        self.__con.commit()

    async def async_update_article_likes(self, article_id):
        await self.loop.run_in_executor(None, self.update_article_likes, article_id)

    def select_favorite_articles(self, user_id: int) -> list:
        headers = self._get_headers('articles')
        result = []
        favorite_list = []
        try:
            self.__cur.execute(f'''SELECT articles_id FROM favorite_articles WHERE telegram_id={user_id};''')
            result = self.__cur.fetchall()
        except Error as e: print('select favorite articles', e)
        if result:
            result = [article_id[0] for article_id in result]
            try:
                self.__cur.execute(f'''SELECT * FROM articles WHERE id IN ({', '.join(map(str, result))});''')
                favorite_list = self.__cur.fetchall()
            except Error as e: print('select articles', e)

        return [dict(zip(headers, row)) for row in favorite_list] if favorite_list else []

    async def async_select_favorite_articles(self, user_id) -> list:
        return await self.loop.run_in_executor(None, self.select_favorite_articles, user_id)

    def delete_article_from_favorite(self, user_id, article_id) -> None:
        try:
            self.__cur.execute(f'''DELETE FROM favorite_articles WHERE articles_id={article_id} 
                                AND telegram_id={user_id}''')
        except Error as e: print('[INFO] delete article from favorite:', e)
        self.__con.commit()

    async def async_delete_article_from_favorite(self, user_id, article_id):
        await self.loop.run_in_executor(None, self.delete_article_from_favorite, user_id, article_id)
